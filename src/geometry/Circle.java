package geometry;

public class Circle implements Shape{
	private double radius;
	
	//Constructor
	public Circle(double radius){
		this.radius = radius;
	}
	
	//get methods
	public double getRadius() {
		return this.radius;
	}
	
	
	public double getArea() {
		double SquareRadius = this.radius * this.radius;
		double area = SquareRadius*Math.PI;
		return area;
	}
	
	public double getPerimeter() {
		double perimeter = 2 * this.radius * Math.PI;
		return perimeter;
	}
}
