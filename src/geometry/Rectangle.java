package geometry;

public class Rectangle implements Shape{
	private double length;
	private double width;
	
	//Constructor
	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	//Get methods
	public double getLength() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	//methods for implementing interface
	public double getArea() {
		double area = this.length * this.width;
		return area;
	}
	
	public double getPerimeter() {
		double perimeter = (this.length*2) + (this.width*2);
		return perimeter;
	}
}
