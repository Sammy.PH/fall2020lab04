package inheritance;

public class Book {
	protected String title;
	private String author;
	
	//Constructor
	public Book(String title, String author){
		this.title = title;
		this.author = author;
	}
	//Get methods
	public String getTitle() {
		return this.title;
	}
	
	public String getAuthor() {
		return this.author;
	}
	
	//override toString
	public String toString() {
		return "title of the book: " + this.title + " Author of the book: " + this.author;
	}
}
