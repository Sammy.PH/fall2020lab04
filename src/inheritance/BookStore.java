package inheritance;

public class BookStore {
	public static void main(String[] args) {
		Book[] book = new Book[5];
		book[0] = new Book("first title", "first author");
		book[1] = new ElectronicBook("second title", "second author", 5);
		book[2] = new Book("third title", "third author");
		book[3] = new ElectronicBook("fourth title", "fourth author", 6);
		book[4] = new ElectronicBook("fifth title", "fifth author", 7);
		
		for(int i = 0; i < book.length; i++) {
			System.out.println(book[i].toString());
		}
	}
}
