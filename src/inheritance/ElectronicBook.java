package inheritance;
public class ElectronicBook extends Book {
	private int numberOfBytes;
	//Constructor
	public ElectronicBook(String title, String author, int numberOfBytes) {
		super(title, author);
		this.numberOfBytes = numberOfBytes;
	}
	
	//override toString
	public String toString() {
		String fromBase = super.toString();
		return fromBase + " size: " + numberOfBytes;
	}
}
